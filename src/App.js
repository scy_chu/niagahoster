import React from "react";
import Routes from "./routes/Routes.js";
import "./App.css";
import LayananNiagahoster from "./Home/componentHome/LayananNiagahoster";

class App extends React.Component{
  render(){
    return (
      <div className="App">
        <Routes/>
      </div>
    )
  }
}

export default App;