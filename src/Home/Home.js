import React from "react";
import FirstSection from "./componentHome/FirstSection";
import Garansi from "./componentHome/Garansi";
import PaymentMethod from "./componentHome/PaymentMethod";
import Quotes from "./componentHome/Quotes";
import LayananNiagahoster from "./componentHome/LayananNiagahoster";
import Benefits from "./componentHome/Benefits";
import QualityPrice from "./componentHome/QualityPrice";
import Testimoni from "./componentHome/Testimoni";
import PaketHosting from "./componentHome/PaketHosting";
import TrustedBy from "./componentHome/TrustedBy";
import Questions from "./componentHome/Questions";

class Home extends React.Component{
  state = {
    layanan: [
        {
            id:1,
            image: require("../assets/images/icon-1.svg"),
            name:"Unlimited Hosting",
            description: "Cocok untuk website skala kecil dan menengah",
            tag: "Mulai dari",
            price:"Rp 10.000,-"
        },
        {
            id:2,
            image: require("../assets/images/icons-cloud-hosting.svg"),
            name:"Cloud Hosting",
            description: "Kapasitas resource tinggi, fully managed dan mudah dikelola",
            tag: "Mulai dari",
            price:"Rp 10.000,-"
        },
        {
            id:3,
            image: require("../assets/images/icons-cloud-vps.svg"),
            name:"Cloud VPS",
            description: "Dedicated resource dengan akses root dan konfigurasi mandiri",
            tag: "Mulai dari",
            price:"Rp 104.000,-"
      },
      {
          id:4,
          image: require("../assets/images/icons-domain.svg"),
          name:"Domain",
          description: "Temukan nama domain yang Anda inginkan",
          tag: "Mulai dari",
          price:"Rp 14.000,-"
    }
    ],
    keuntungan: [
      {
          id:1,
          image: require("../assets/images/icons-domain.svg"),
          description: "Pengunjung tidak suka website lambat. Dengan dukungan LiteSpeed Web Server, waktu loading website Anda akan meningkat pesat.",
          name: "Hosting Super Cepat",
      },
      {
          id:2,
          image: require("../assets/images/icons-domain.svg"),
          description: "Teknologi keamanan Imunify 360 memungkinkan website Anda terlindung dari serangan hacker, malware, dan virus berbahaya setiap saat.",
          name: "Keamanan Website Ekstra",
    }
    ],
    kualitas: [
      {
          id:1,
          image: require("../assets/images/icon-online.svg"),
          description: "Anda bisa berhemat dan tetap mendapatkan hosting terbaik dengan fitur lengkap, dari auto install WordPress, cPanel lengkap, hingga SSL gratis",
          name: "Harga Murah, Fitur Lengkap"
      },
      {
          id:2,
          image: require("../assets/images/icon-online.svg"),
          description: "Jaminan server uptime 99,98% memungkinkan website Anda selalu online sehingga Anda tidak perlu khawatir kehilangan trafik dan pendapatan.",
          name: "Website Selalu Online"
    },
      {
          id:3,
          image: require("../assets/images/icon-online.svg"),
          description: "Tidak perlu menunggu lama, selesaikan masalah Anda dengan cepat secara real time melalui live chat 24/7",
          name: "Tim Support Andal dan Cepat Tanggap"
       }
    ],
    testimoni: [
      {
          id:1,
          video: "https://www.youtube.com/embed/m7Friw3UJAQ",
          description: "Website itu sangat penting bagi UMKM sebagai sarana promosi untuk memenangkan persaingan di era digital.",
          name: "Didik & Johan",
          title:"Owner Devjavu"
      },
      {
          id:2,
          video:"https://www.youtube.com/embed/p9xerYbmaTI",
          description: " Bagi saya Niagahoster bukan sekadat penyedia hosting, melainkan partner bisnis yang bisa dipercaya.",
          name: "Bob Setyo",
          title:"Owner Digital Optimizer Indonesia"
    },
      {
          id:3,
          video:"https://www.youtube.com/embed/y4nMPkeqtOg",
          description: "Solusi yang diberikan tim support Niagahoster sangat mudah dimengerti buat saya yang tidak paham teknis.",
          name: "Budi Seputro",
          title:"Owner Sate Ratu"
       }
    ],
    paket: [
      {
        id:1,
        label:"Termurah!",
        kategori:"Bayi",
        promo:"Rp 10.000",
        bulan:"/bln",
        description: "Sesuai untuk Pemula atau Belajar Website",
        list: [
          "500MB Disk Space",
          "Unlimited Bandwidth"
        ]
    },
    {
        id:2,
        label:"Diskon up to 34%",
        kategori:"Pelajar",
        harga:"Rp 60.800,-",
        promo: "Rp 40.223",
        bulan:"/bln",
        description: "Sesuai untuk Budget Minimal, Landing Page, Blog Pribadi",
  },
    {
        id:1,
        label:"Diskon up to 75%",
        kategori:"Personal",
        harga:"Rp 106.250,-",
        promo: "Rp 26.563",
        bulan:"/bln",
        description: "Sesuai untuk Website Bisnis, UKM, Organisasi, Komunitas, Toko Online, dll",
     },
     {
        id:1,
        label:"Diskon up to 42%",
        kategori:"Bisnis",
        harga:"Rp 147.800,-",
        promo: "Rp 85.724",
        bulan:"/bln",
        description: "Sesuai untuk Website Bisnis, Portal Berita, Toko Online, dll",
   }
    ]
}
  
  render(){
    return (
      <div className="home">
          <FirstSection/>
          <LayananNiagahoster dataLayanan={this.state.layanan}/>
          <Benefits dataBenefit={this.state.keuntungan}/>
          <QualityPrice dataQuality={this.state.kualitas}/>
          <Testimoni dataTestimoni={this.state.testimoni}/>
          <PaketHosting dataPackage={this.state.paket}/>
          <TrustedBy/>
          <Questions/>
          <Garansi/>
          <PaymentMethod/>
          <Quotes/>
      </div>
    )
  }
}
export default Home;