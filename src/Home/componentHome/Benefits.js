import React from "react";
import "../../assets/style/Benefits.css";

const Benefits =(props) => {
    let keuntungan = props.dataBenefit.map(item =>
        <div className="benefit" key={item.id}>
            <img src={item.image} alt={item.name} className="benefit-image"/>
            <h3>{item.name}</h3>
            <p>{item.description}</p>
        </div>
    )
    return(
        <div className="benefits-wrapper">
           <h1 className="benefits-h1">Prioritas Kecepatan dan Keamanan</h1>
           <div className="benefits-container">
                <div className="benefit-text">
                    {keuntungan}
                    <button className="btn-orange">LIHAT SELENGKAPNYA</button>
                </div>
                <div className="benefit-img">
                    <img className="main-img" src={require("../../assets/images/server.webp")} alt="server" />
                    <img className="img img1" src={require("../../assets/images/imunify.svg")} alt="imunify" />
                    <img
                        class="img img2"
                        src={require("../../assets/images/lite-speed.svg")}
                        alt="lite speed"
                    />
                    <img class="img img3" src={require("../../assets/images/graphic.svg")} alt="grafik" />
                </div>
           </div>
           
        </div>
    )
}
        
export default Benefits;