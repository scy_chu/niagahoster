import React from "react";
import "../../assets/style/FirstSection.css"

const FirstSection = () => {
    return (
        <div className="main-container">
          <div className="main">
            <div className="main-text">
              <h1>Unlimited Web Hosting Terbaik di Indonesia</h1>
              <p>
                Ada banyak peluang bisa Anda raih dari rumah dengan memiliki
                website. Manfaatkan diskon hosting hingga 75% dan tetap produktif di
                bulan Ramadhan bersama Niagahoster.
              </p>
              <p>Yuk segera order karena diskon dapat berakhir sewaktu-waktu!</p>
              <div className="main-time">
                <div className="time-day">
                  <h1>0</h1>
                  <p>Hari</p>
                </div>
                <div className="divide">
                  <h1>:</h1>
                </div>
                <div className="time-hour">
                  <h1>4</h1>
                  <p>Jam</p>
                </div>
                <div className="divide">
                  <h1>:</h1>
                </div>
                <div className="time-minute">
                  <h1>32</h1>
                  <p>Menit</p>
                </div>
                <div className="divide">
                  <h1>:</h1>
                </div>
                <div className="time-second">
                  <h1>31</h1>
                  <p>Detik</p>
                </div>
              </div>
              <button className="btn-orange">PILIH SEKARANG</button>
            </div>
            <div className="main-img">
              <img src={require("../../assets/images/hero-home-ramadhan.webp")} alt="" />
            </div>
          </div>
        </div>
    )
}

export default FirstSection;