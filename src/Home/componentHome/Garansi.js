import React from "react";
import "../../assets/style/Garansi.css";

const Garansi = ()=> {
    return (
        <div className="guaranted-container">
            <div className="garansi">
                <div className="guaranted-img">
                    <img src={require("../../assets/images/icons-guarantee.svg")} alt="guarantee" />
                </div>
                <div className="guaranted-text">
                    <h1>Garansi 30 Hari Uang Kembali</h1>
                    <p>
                    Tidak puas dengan layanan hosting Niagahoster? Kami menyediakan
                    garansi uang kembali yang berlaku 30 hari sejak tanggal pembelian.
                    </p>
                    <button className="btn-orange">MULAI SEKARANG</button>
                </div>
            </div>
      </div>

    )
}

export default Garansi;