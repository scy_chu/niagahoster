import React from "react";
import "../../assets/style/LayananNiagahoster.css"; 

const LayananNiagahoster =(props) => {
    let lists = props.dataLayanan.map(item =>
        <div className="service" key={item.id}>
            <img src={item.image} alt={item.name} className="service-image"/>
            <h3>{item.name}</h3>
            <p>{item.description}</p>
            <h5>{item.tag}</h5>
            <h2>{item.price}</h2>
        </div>
    )
    return(
        <div className="services-container">
           <h1>LayananNiagahoster</h1>
           <div className="services-cards">
                {lists}
                <div class="service srv5">
                <img
                    src={require("../../assets/images/home-pembuatan-website.svg")}
                    alt="buat website"
                />
                <h3>Pembuatan website</h3>
                <p>
                    500 perusahaan lebih percayakan pembuatan websitenya pada kami.
                    <a href="#"> Cek selengkapnya...</a>
                </p>
                </div>
           </div>
        </div>
    )
}
        
export default LayananNiagahoster;