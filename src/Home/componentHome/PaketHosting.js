import React from "react";
import "../../assets/style/PaketHosting.css";


const PaketHosting = (props) => {
    let paketan = props.dataPackage.map(item =>
        <div className="package" key={item.id}>
            <div className="up">
                <h3 className="label">{item.label}</h3>
                <h1 className="kategori">{item.kategori}</h1>
                <h5 className="ori-price">{item.harga}</h5>
                <h2 className={item.promo}>{item.promo}</h2>
                <h6 className={item.bulan}>{item.bulan}</h6>
            </div>
            <div className="down">
                <button className="btn-orange pckb">PILIH SEKARANG</button>
                <p>{item.description}</p>
                <ul>
                    <li>{item.list}</li>
                </ul>
            </div>
        </div>
    )
    return(
        <div className="packages-wrapper">
            <h1 className="packages-title">Pilih Paket Hosting Anda</h1>
            <div className="packages-container">
                {paketan}
            </div>
            
        </div>
    )
}

export default PaketHosting;