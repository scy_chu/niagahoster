import React from "react";
import "../../assets/style/PaymentMethod.css";

const PaymentMethod = ()=> {
    return (
        <div className="payment-container">
            <h4>
                Beragam pilihan cara pembayaran yang mempercepat proses order hosting &
                domain Anda.
            </h4>
            <div className="paymentlogo-container">
                <img src={require("../../assets/images/bca.svg")} alt="bca" />
                <img src={require("../../assets/images/mandiri.svg")} alt="mandiri" />
                <img src={require("../../assets/images/bni.svg")} alt="bni" />
                <img src={require("../../assets/images/bri.svg")} alt="bri" />
                <img src={require("../../assets/images/bii.svg")} alt="bii" />
                <img src={require("../../assets/images/cimb.svg")} alt="cimb" />
                <img src={require("../../assets/images/alto.svg")} alt="alto" />
                <img src={require("../../assets/images/atm-bersama.svg")} alt="atm-bersama" />
                <img src={require("../../assets/images/paypal.svg")} alt="paypal" />
                <img src={require("../../assets/images/indomart.svg")} alt="indomaret" />
                <img src={require("../../assets/images/alfamart.svg")} alt="alfamart" />
                <img src={require("../../assets/images/pegadaian.svg")} alt="pegadaian" />
                <img src={require("../../assets/images/pos.svg")} alt="pos" />
                <img src={require("../../assets/images/ovo.svg")} alt="ovo" />
                <img src={require("../../assets/images/gopay.svg")} alt="gopay" />
                <img src={require("../../assets/images/visa.svg")} alt="visa" />
                <img src={require("../../assets/images/master.svg")} alt="master" />
            </div>
        </div>
    )
}

export default PaymentMethod;