import React from "react";
import "../../assets/style/PromoCampaign.css";

const PromoCampaign = (props) => {
    return(
        <div className="promo-container">
            <div className="promo-campaign">
                <div className="promo-img">
                    <img src={require("../../assets/images/promo-camp.svg")}/>
                </div>
                <div className="promo-text">
                    <h2>Unlimited Hosting</h2>
                    <p>Discount</p>
                    <p> hingga</p>
                    <h1>75%</h1>
                </div>
                <div className="promo-time">
                    <p>Yuk segera order karena diskon dapat berakhir sewaktu-waktu!</p>
                    <h3>03: 02: 01: 59</h3>
                </div>
                <div className="promo-button">
                    <button className="btn-orange">PILIH SEKARANG</button>
                </div>
            </div>
        </div>
    )
}

export default PromoCampaign;

