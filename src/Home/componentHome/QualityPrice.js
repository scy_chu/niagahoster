import React from "react";
import "../../assets/style/QualityPrice.css";

const QualityPrice =(props) => {
    let kualitas = props.dataQuality.map(item =>
        <div className="price" key={item.id}>
            <img src={item.image} alt={item.name} className="price-image"/>
            <h3>{item.name}</h3>
            <p>{item.description}</p>
        </div>
    )
    return(
        <div className="prices-wrapper">
           <h1 className="prices-h1">Prioritas Kecepatan dan Keamanan</h1>
           <div className="prices-container">
                <div className="prices-img">
                    <img className="main-img" src={require("../../assets/images/cs.png")} alt="customer service" />
                    <img className="img img-p1" src={require("../../assets/images/intercom-logo.svg")} alt="intercom" />
                    <img className="img img-p2" src={require("../../assets/images/woman1.webp")} alt="cswoman"/>
                    <img className="img img-p3" src={require("../../assets/images/woman2.png")} alt="cswoman2"/>
                    <img className="img img-p4" src={require("../../assets/images/man.png")} alt="csman"/>
                    <img className="img img-p5" src={require("../../assets/images/icon-online.svg")} alt="online"/>
                </div>
                <div className="prices-text">
                    {kualitas}
                </div>
           </div>
        </div>
    )
}
        
export default QualityPrice;