import React from "react";
import "../../assets/style/Questions.css";

const Questions = () => {
    return (
        <div class="qna">
            <h1 class="qna-title">Pertanyaan yang Sering Diajukan</h1>
            <div class="qna-corner">
            <a href="#ans1" class="question">
                <h3>
                <li id="que1">Apa itu web hosting?</li>
                </h3>
            </a>
            <p class="ans" id="ans1">
                Web hosting adalah layanan penyimpanan data agar suatu website dapat
                diakses secara online. Data website ini ditampung dalam ruang
                penyimpanan bernama server web hosting yang selalu aktif 24 jam setiap
                hari. Kualitas layanan web hosting dapat menentukan kesuksesan bisnis
                maupun segala aktivitas website Anda. Tanpa layanan web hosting yang
                berkualitas, suatu website tidak mungkin dapat diakses dengan baik.
                Oleh karena itu, selalu gunakan layanan web hosting terbaik untuk
                website Anda
            </p>
            <a href="#ans2" class="question">
                <h3>
                <li id="que2">
                    Mengapa saya harus menggunakan web hosting Indonesia?
                </li>
                </h3>
            </a>
            <p class="ans" id="ans2">
                Web hosting Indonesia adalah layanan hosting dengan server yang
                berlokasi di Indonesia. Lokasi ini sangat berpengaruh terhadap
                performa suatu website, terutama jika target pengunjung website Anda
                juga berada di dalam wilayah Indonesia. Saat ini Niagahoster
                menggunakan Green Data Center Tier-4 DCI Indonesia berstandar
                internasional. Data center yang kami gunakan merupakan bagian dari
                Equinix, penyedia data center berkualitas tinggi terbaik di dunia
                dengan jaminan uptime hingga 99,98%
            </p>
            <a href="#ans3" class="question">
                <h3>
                <li id="que3">
                    Apa yang dimaksud dengan Unlimited Hosting di Niagahoster?
                </li>
                </h3>
            </a>
            <p class="ans" id="ans3">
                Unlimited Hosting Niagahoster tidak menetapkan batasan khusus dalam
                hal resource atau sumber daya website yang Anda butuhkan. Meskipun
                demikian, kami menetapkan Fair Usage Policy (FUP) atau kebijakan
                penggunaan secara adil yang berlaku untuk semua akun yang menggunakan
                paket Bayi, Pelajar, Personal, serta Bisnis. Penggunaan secara wajar
                sangat kami rekomendasikan, mengingat penggunaan resource CPU, RAM,
                serta disk space secara berlebihan dalam sebuah server dapat berdampak
                pada performa website milik pengguna lain yang tersimpan dalam server
                yang sama. Cek fitur Hosting Unlimited
            </p>
            <a href="#ans4" class="question">
                <h3>
                <li id="que4">Paket web hosting mana yang tepat untuk saya?</li>
                </h3>
            </a>
            <p class="ans" id="ans4">
                Niagahoster menawarkan beragam paket hosting yang bisa Anda pilih
                sesuai kebutuhan. Oleh karena itu, kami sangat menyarankan Anda untuk
                mengetahui kebutuhan Anda terlebih dahulu. Apakah itu website company
                profile, blog pribadi, atau toko online. Tiap jenis website ini
                memerlukan keperluan resource yang beragam. Sebagai contoh, jika Anda
                menginginkan website instan sederhana dan langsung online, Anda bisa
                menggunakan paket Unlimited Hosting. Kemudian, untuk website dengan
                fitur yang bisa dikustomisasi sendiri, Anda dapat menggunakan Cloud
                VPS. Apabila Anda membutuhkan web hosting dengan dedicated server yang
                fully managed, Anda bisa memanfaatkan Cloud Hosting. Soal bantuan,
                Anda tidak perlu khawatir. Tim support kami siap membantu Anda kapan
                saja!
            </p>
            <a href="#ans5" class="question">
                <h3>
                <li id="que5">
                    Apakah semua pembelian hosting mendapatkan domain gratis?
                </li>
                </h3>
            </a>
            <p class="ans" id="ans5">
                Hanya di Niagahoster Anda bisa mendapatkan domain gratis dengan
                membeli web hosting. Gratis domain hanya berlaku untuk pembelian paket
                Unlimited Hosting (Pelajar, Personal, dan Bisnis) dan semua paket
                Cloud Hosting. Anda tinggal pilih, arahkan ke akun hosting, dan domain
                gratis Anda siap digunakan.
            </p>
            <a href="#ans6" class="question">
                <h3>
                <li id="que6">
                    Jika sudah memiliki website, apakah saya bisa transfer web hosting
                    ke Niagahoster?
                </li>
                </h3>
            </a>
            <p class="ans" id="ans6">
                Transfer hosting tidaklah rumit. Tim support kami siap membantu
                memindahkan hosting Anda ke Niagahoster kapan pun Anda inginkan.
                Caranya, Anda hanya perlu mengisi formulir dan permintaan transfer
                akan kami proses secepatnya. Transfer hosting Anda ke layanan
                Niagahoster sekarang dan dapatkan diskon hingga 50%! Silakan hubungi
                tim support kami untuk mendapatkan informasi lebih lengkap.
            </p>
            <a href="#ans7" class="question">
                <h3>
                <li id="que7">Apa saja layanan web hosting Niagahoster?</li>
                </h3>
            </a>
            <div class="ans" id="ans7">
                <h4>Unlimited Hosting</h4>
                Unlimited hosting merupakan layanan hosting Niagahoster yang paling
                laris. Niagahoster menawarkan harga yang sangat terjangkau untuk paket
                ini, mulai dari Rp8.000,- per bulan.
    
                <h4>Cloud VPS</h4>
                Selain hosting murah unlimited, Niagahoster juga memiliki paket
                Virtual Private Server (VPS). Di sini Anda memiliki keleluasaan
                membangun website sendiri hingga level konfigurasi teknis servernya.
                Paket Cloud VPS Niagahoster ini bisa Anda dapatkan mulai Rp 134.000,-
                per bulan.
    
                <h4>Cloud Hosting</h4>
                Nikmati performa dedicated server yang bisa menampung trafik tinggi
                dengan pengelolaan mudah seperti shared hosting. Didukung LiteSpeed,
                WordPress Accelerator, dan domain gratis, website Anda bisa memberikan
                performa terbaik dari kecepatan dan daya tampung.
            </div>
            </div>
      </div>
    )
}

export default Questions;