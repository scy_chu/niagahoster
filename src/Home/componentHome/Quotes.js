import React from "react";
import "../../assets/style/Quotes.css";

const Quotes = ()=> {
    return (
        <div className="quotes-container">
            <div className="quotes">
                <div className="quote-text">
                    <h1>Awali kesuksesan online Anda bersama Niagahoster!</h1>
                </div>
                <div className="quote-button">
                    <button className="btn-orange quote-btn">MULAI SEKARANG</button>
                </div>
            </div>
        </div>
    )
}

export default Quotes;