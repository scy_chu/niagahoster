import React from "react";
import "../../assets/style/Testimoni.css";

const Testimoni =(props)=> {
    let testimoni = props.dataTestimoni.map(item =>
        <div className="video" key={item.id}>
            <iframe src={item.video} frameBorder="0"/>
            <p>{item.description}</p>
            <h4>{item.name} - {item.title}</h4>
        </div>
    )
        return(
            <div className="customer-testimoni">
                <h1>Kata Pelanggan Tentang Niagahoster</h1>
                <div className="videos-container">
                    {testimoni}
                </div>
            </div>
        )
}


export default Testimoni;