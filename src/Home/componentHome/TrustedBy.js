import React from "react";
import "../../assets/style/TrustedBy.css";

const PaymentMethod = ()=> {
    return (
        <div className="trustedby">
            <h1 className="trustedby-title">
                Dipercaya 52.000+ Pelanggan di Seluruh Indonesia
            </h1>

            <div className="logos-container">
                <img src={require("../../assets/images/richeese.png")} alt="richeese" />
                <img src={require("../../assets/images/rabbani.jpeg")} alt="rabbani" />
                <img src={require("../../assets/images/otten.jpeg")} alt="otten" />
                <img src={require("../../assets/images/hydrococo.png")} alt="hydrococo" />
                <img src={require("../../assets/images/petrokimia.png")} alt="petrokimia" />
            </div>
        </div>
    )
}

export default PaymentMethod;