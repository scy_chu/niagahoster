import React from "react";
import "../assets/style/Footer.css";

const Footer = () => {
  return (
    <div className="footer-container">
      <div className="foot">
        <div className="foot-menu">
          <h3>Hubungi Kami</h3>
          <p>Telp: <a href="#">0274-2885822</a></p>
          <p>WA: <a href="#">0895422447394</a></p>
          <p>Senin - Minggu</p>
          <p>24 Jam Non Stop</p>
          <div className="alamat">
            <p>Jl. Palangan Tentara Pelajar</p>
            <p>No 81 Jongkang, Sariharjo,</p>
            <p>Ngaglik, Sleman</p>
            <p>Daerah Istimewa Yogyakarta</p>
            <p>55581</p>
          </div>
        </div>
        <div className="foot-menu">
          <h3>Layanan</h3>
            <li>Domain</li>
            <li>Shared Hosting</li>
            <li>Cloud Hosting</li>
            <li>Cloud VPS Hosting</li>
            <li>Transfer Hosting</li>
            <li>Web Builder</li>
            <li>Keamanan SSL/HTTPS</li>
            <li>Jasa Pembuatan Website</li>
            <li>Program Afiliasi</li>
            <li>Whois</li>
            <li>Niagahoster Status</li>
        </div>
        <div className="foot-menu">
          <h3>Service Hosting</h3>
          <li><a href="#">Hosting Murah</a></li>
          <li><a href="#">Hosting Indonesia</a></li>
          <li><a href="#">Hosting Singapore SG</a></li>
          <li><a href="#">Hosting Wordpress</a></li>
          <li><a href="#">Email Hosting</a></li>
          <li><a href="#">Reseller Hosting</a></li>
          <li><a href="#">Web Hosting Unlimited</a></li>
        </div>
        <div className="foot-menu">
          <h3>Kenapa Pilih Niagahoster?</h3>
          <li><a href="#">Hosting Terbaik</a></li>
          <li><a href="#">Datacenter Hosting Terbaik</a></li>
          <li><a href="#">Domain Gratis</a></li>
          <li><a href="#">Bagi-bagi Domain Gratis</a></li>
          <li><a href="#">Bagi-bagi Hosting Gratis</a></li>
          <li><a href="#">Review Pelanggan</a></li>
        </div>
        <div className="foot-menu">
          <h3>Tutorial</h3>
          <li><a href="#">Ebook Gratis</a></li>
          <li><a href="#">Knowledgebase</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Cara Pembayaran</a></li>
          <li><a href="#">Niaga Course</a></li>
        </div>
        <div className="foot-menu">
          <h3>Tentang Kami</h3>
          <li><a href="#">Tentang</a></li>
          <li><a href="#">Penawaran & Promo Spesial</a></li>
          <li><a href="#">Niaga Poin</a></li>
          <li><a href="#">Karir</a></li>
          <li><a href="#">Kontak Kami</a></li>
        </div>
        <div className="foot-menu">
          <h3>Newsletter</h3>
          <form>
            <input type="text" placeholder="yourmail.gmail.com" />
            <button className="btn-orange">BERLANGGANAN</button>
          </form>
        </div>
        <div className="foot-menu">
          <div className="sosmed-logo">
            <img src={require("../assets/images/facebook_icon.png")} alt="fb" />
            <img src={require("../assets/images/instagram_icon.png")} alt="instagram" />
            <img src={require("../assets/images/linkedin_icon.png")} alt="linkedin" />
            <img src={require("../assets/images/twitter_icon.png")} alt="twitter" />
          </div>
        </div>
        <div className="foot-text">
          <div className="ft-left">
            <p>
              Copyright ©2019 Niagahoster | Hosting powered by PHP7, CloudLinux,
              CloudFlare, BitNinja and DC DCI-Indonesia. Cloud VPS Murah powered
              by Webuzo Softaculous, Intel SSD and cloud computing technology
            </p>
          </div>
          <div className="ft-right">
            <li><a href="#">Syarat dan Ketentuan |</a></li>
            <li><a href="#">Kebijakan Privasi</a></li>
          </div>
        </div>
      <div className="chatme">
      <img src={require("../assets/images/chatme.png")} alt="chat" />
      </div>
      </div>
    </div>
  )
}

export default Footer;