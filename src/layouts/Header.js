import React from "react";
import {Link} from "react-router-dom";
import "../assets/style/Header.css";

const Header = () => {
  return (
    <div className="header-container">
      <div className="mini-info">
        <ul>
          <li><img src={require("../assets/images/call.png")}alt="call"/>0274-2885822</li>
          <li> <img src={require("../assets/images/chat.png")}alt="chat"/>Live Chat</li>
          <li> <img src={require("../assets/images/cart.png")}alt="cart"/></li>
        </ul>
      </div>
      <div className="logo-container">
        <div className="logo">
          <Link to="/"><img className="logo-img" src={require("../assets/images/nh-logo.svg")} alt="logo"/></Link>
        </div>
        <div className="nav-bar">
          <ul>
            <li><Link className="linked-page"to="/unlimited-hosting">UNLIMITED HOSTING</Link></li>
            <li><Link className="linked-page"to="/cloud-hosting">CLOUD HOSTING</Link></li>
            <li><Link className="linked-page"to="/cloud-vps">CLOUD VPS</Link></li>
            <li><Link className="linked-page"to="/domain">DOMAIN</Link></li>
            <li><Link className="linked-page"to="/afiliasi">AFILIASI</Link></li>
            <li><Link className="linked-page"to="/blog">BLOG</Link></li>
            <button className="login-btn">LOGIN</button>
          </ul>
        </div>
      </div>

    </div>       
    )
}

export default Header;