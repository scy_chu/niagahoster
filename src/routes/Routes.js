import React from "react";
// import About from "../views/About.js";
import Sign from "../views/Sign.js";
import Footer from "../layouts/Footer.js";
import Header from "../layouts/Header.js";
import {Route} from "react-router-dom";
import Home from "../Home/Home.js";
import UnlimitedHosting from "../views/UnlimitedHosting";
import PromoCampaign from "../Home/componentHome/PromoCampaign";
import CloudHosting from "../views/CloudHosting";
import CloudVPS from "../views/CloudVPS";
import Domain from "../views/Domain";
import Afiliasi from "../views/Afiliasi";
import Blog from "../views/Blog";
import PropsPage from "./PropsPage.js";

const Routes = ()=> {
    return (
        <div>
            <Header/>
            <Route path="/" exact component={Home}/>
            <Route path="/unlimited-hosting"exact>
                <UnlimitedHosting/>
                <PromoCampaign/>
            </Route>
            <Route path="/cloud-hosting" exact>
                <CloudHosting/>
                <PromoCampaign/>
            </Route>
            <Route exact path="/props-through" 
                render={(props)=> <PropsPage {...props} 
                title={"Apasih kok gak kebaca"} />} 
            />
            <Route path="/cloud-vps" exact>
                <CloudVPS/>
                <PromoCampaign/>
            </Route>
            <Route path="/domain" exact>
                <Domain/>
            </Route>
            <Route path="/afiliasi" exact>
                <Afiliasi/>
            </Route>
            <Route path="/blog" exact>
                <Blog/>
            </Route>
            <Route path="/sign" component={Sign}exact></Route>
            <Footer/>
        </div>
    )
}

export default Routes;