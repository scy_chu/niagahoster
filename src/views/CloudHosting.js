import React from "react";
import "../assets/style/CloudHosting.css";

const CloudHosting = () => {
    return (
        <div className="cloudhost-container">
                <div className="main-cloudhost">
                    <div className="cloudhost-text">
                        <h1>Cloud Hosting Indonesia</h1>
                        <p>
                        Cloud hosting terbaik siap mendukung kesuksesan bisnis Anda. Mudah digunakan, cepat diakses, selalu online!
                        </p>
                        <button className="btn-orange">PILIH SEKARANG</button>
                    </div>
                <div className="cloudhost-img">
                    <img src={require("../assets/images/cloud-host.svg")} alt="cloud hosting" />
                </div>
            </div>
        </div>
    )
}

export default CloudHosting;