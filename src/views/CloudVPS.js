import React from "react";
import "../assets/style/CloudVPS.css";

const CloudVPS = () => {
    return (
        <div className="cloudvps-container">
                <div className="main-cloudvps">
                    <div className="cloudvps-text">
                        <h1>Saatnya Pindah ke VPS Murah Berkualitas!</h1>
                        <p>
                        Dapatkan kontrol penuh atas server dan hosting yang Anda kelola dengan akses full root di VPS Hosting. Upgrade resource kapan saja sesuai kebutuhan Anda untuk menampung trafik website super tinggi!
                        </p>
                        <button className="btn-orange">PILIH SEKARANG</button>
                    </div>
                <div className="cloudvps-img">
                    <img src={require("../assets/images/cloud-host.svg")} alt="cloud hosting" />
                </div>
            </div>
        </div>
    )
}

export default CloudVPS;