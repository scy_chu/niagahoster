import React from "react";
import "../assets/style/Domain.css";
const Domain = () => {
    return (
        <div className="domain-container">
            <div className="domain-wrapper">
                <div className="domain-text">
                    <h1>Cek Domain dan Beli Domain Murah Anda!</h1>
                    <p>
                    Sedang mencari nama domain? Cek nama domain dan temukan domain
                    impian Anda hanya dengan satu klik!
                    </p>
                </div>
                <div className="search-box">
                    <input className="searchbar" type="text" placeholder={"Cari Nama Domain Anda"} />
                    <button className="domain-type">.com</button>
                    <button className="btn-orange cek">CEK DOMAIN</button>
                </div>
                <div className="domain-notes">
                    <p>.com Rp125.000,- |</p>
                    <p>.xyz Rp 18.000,- |</p>
                    <p>.net Rp155.000,- |</p>
                    <p>.online Rp 15.000,- |</p>
                    <p>.tech Rp 30.000,- |</p>
                    <p>.store Rp30.000,-</p>
                </div>
            </div>
        </div>

    )
}

export default Domain;