import React from "react";
import "../assets/style/UnlimitedHosting.css";

const UnlimitedHosting = ()=> {
    return (
        <div className="unli-container">
                <div className="main-unli">
                    <div className="unli-text">
                        <h1>Unlimited Web Hosting Indonesia</h1>
                        <p>
                        Layanan web hosting Indonesia terbaik dengan fitur hosting terlengkap dan dukungan support 24 jam untuk kemudahan Anda meraih kesuksesan online!
                        </p>
                        <button className="btn-orange">PILIH SEKARANG</button>
                    </div>
                <div className="unli-img">
                    <img src={require("../assets/images/unli-host.svg")} alt="unlimited hosting" />
                </div>
            </div>
        </div>
    )
}

export default UnlimitedHosting;